object DateInputPair: TDateInputPair
  Left = 0
  Top = 0
  Width = 171
  Height = 187
  TabOrder = 0
  object Header: TLabel
    Left = 0
    Top = 2
    Width = 16
    Height = 13
    Caption = #1054#1090':'
  end
  object Editor: TDateEdit
    Left = 24
    Top = 0
    Width = 136
    Height = 19
    ButtonWidth = 0
    NumGlyphs = 2
    TabOrder = 0
    OnExit = EditorExit
    OnKeyPress = EditorKeyPress
  end
  object Calendar: TMonthCalendar
    Left = 0
    Top = 24
    Width = 162
    Height = 153
    AutoSize = True
    Date = 0.606995902780909100
    TabOrder = 1
    OnClick = CalendarClick
    OnDblClick = CalendarDblClick
  end
end
