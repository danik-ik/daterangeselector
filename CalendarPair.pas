unit CalendarPair;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ComCtrls, StdCtrls, Mask, ToolEdit;

type
  TDatePairEvent = procedure(BeginDate, EndDate: TDate) of object;
  TDateEvent = procedure(Date: TDate) of object;

  TDateInputPair = class(TFrame)
    Header: TLabel;
    Editor: TDateEdit;
    Calendar: TMonthCalendar;
    procedure EditorExit(Sender: TObject);
    procedure EditorKeyPress(Sender: TObject; var Key: Char);
    procedure CalendarClick(Sender: TObject);
    procedure CalendarDblClick(Sender: TObject);
  private
    FDate: TDate;
    FOnSelectRange: TDatePairEvent;
    FOnSelect: TDateEvent;
    FOnCalendarDblClick: TNotifyEvent;
    procedure SetDate(const Value: TDate);
    procedure ApplyEditorDate();
    procedure ApplyCalendarDate();
    procedure SetOnSelectRange(const Value: TDatePairEvent);
    procedure SetOnSelect(const Value: TDateEvent);
  public
    property Date: TDate read FDate write SetDate;
    property OnSelectRange: TDatePairEvent read FOnSelectRange write SetOnSelectRange;
    property OnSelect: TDateEvent read FOnSelect write SetOnSelect;
    property OnCalendarDblClick: TNotifyEvent read FOnCalendarDblClick write FOnCalendarDblClick;
  end;

implementation

{$R *.dfm}

procedure TDateInputPair.EditorExit(Sender: TObject);
begin
  ApplyEditorDate();
end;

procedure TDateInputPair.SetDate(const Value: TDate);
begin
  FDate := Value;
  Editor.Date := value;
  if (Calendar.Date <> value)
  or (Calendar.EndDate <> value) then // ��� ����� ������� �� ����������� ��������
  begin
    Calendar.MultiSelect := false; // ��� ����� ����� �������� �� ������������� ��������
    Calendar.Date := value;
    Calendar.MultiSelect := true;
  end;
end;

procedure TDateInputPair.EditorKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    ApplyEditorDate();
end;

procedure TDateInputPair.ApplyEditorDate;
begin
  if Editor.Date = 0 then begin
    Editor.SetFocus;
    ShowMessage('���������� ������ ���������� ����');
    exit;
  end;
  if self.Date = Editor.Date then
    exit;
  self.Date := Editor.Date;
  if Assigned(self.OnSelect) then
    self.OnSelect(Calendar.Date);
end;

procedure TDateInputPair.CalendarClick(Sender: TObject);
begin
  ApplyCalendarDate;
end;

procedure TDateInputPair.ApplyCalendarDate;
begin
  if Calendar.Date = Calendar.EndDate then
  begin
    if Calendar.Date = self.Date then
      exit;
    self.Date := Calendar.Date;
    if Assigned(self.OnSelect) then
      self.OnSelect(Calendar.Date);
  end
  else
    if Assigned(self.OnSelectRange) then
      self.OnSelectRange(Calendar.Date, Calendar.EndDate);
    // �������� �������� ������� ���������� ���� �� ����
end;

procedure TDateInputPair.SetOnSelectRange(const Value: TDatePairEvent);
begin
  FOnSelectRange := Value;
end;

procedure TDateInputPair.CalendarDblClick(Sender: TObject);
begin
  // ���������� ������ ���� ����
  if Assigned(self.OnSelectRange) then
    self.OnSelectRange(Calendar.Date, Calendar.Date);
  if Assigned(FOnCalendarDblClick) then
    FOnCalendarDblClick(self);
end;

procedure TDateInputPair.SetOnSelect(const Value: TDateEvent);
begin
  FOnSelect := Value;
end;

end.
