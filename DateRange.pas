unit DateRange;

interface
uses
  Controls;

type
  TDateRange = object
  public
    BeginDate, EndDate: TDate;
    function incDay(shift: integer): TDateRange;
    function AsStr: string;
  end;

implementation
uses
  SysUtils, DateUtils;

{ TDateRange }

function TDateRange.AsStr: string;
var s: string;
begin
  result := FormatDateTime('DD.MM.YYYY', BeginDate);
  s := FormatDateTime('DD.MM.YYYY', EndDate);
  if s <> result then
    Result := Result + ' � ' + s;
end;

function TDateRange.incDay(shift: integer): TDateRange;
begin
  result.BeginDate := DateUtils.IncDay(BeginDate, shift);
  result.EndDate := DateUtils.IncDay(EndDate, shift);
end;

end.
