unit DateRangeDisplayFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, DateRange;

type
  TDateRangeDisplay = class(TFrame)
    Display: TMemo;
    procedure DisplayClick(Sender: TObject);
  private
    FRange: TDateRange;
    FOnChanged: TNotifyEvent;
    procedure SetBeginDate(const Value: TDate);
    procedure SetEndDate(const Value: TDate);
    procedure SetRange(const Value: TDateRange);
    procedure RefreshDisplay();
    procedure SelectNewRange();
  public
    ReadOnly: boolean;
    constructor Create(AOwner: TComponent); override;
    property Range: TDateRange read FRange write SetRange;
    property BeginDate: TDate read FRange.BeginDate write SetBeginDate;
    property EndDate: TDate read FRange.EndDate write SetEndDate;
    property OnChanged: TNotifyEvent Read FOnChanged write FOnChanged;
  end;

implementation
uses
  SelectDateRangeDlg;

{$R *.dfm}

{ TDateRangeDisplay }

procedure TDateRangeDisplay.RefreshDisplay;
begin
  Display.Text := FRange.AsStr;
end;

procedure TDateRangeDisplay.SetBeginDate(const Value: TDate);
begin
  FRange.BeginDate := Value;
  RefreshDisplay;
end;

procedure TDateRangeDisplay.SetEndDate(const Value: TDate);
begin
  FRange.EndDate := Value;
  RefreshDisplay;
end;

procedure TDateRangeDisplay.SetRange(const Value: TDateRange);
begin
  FRange := Value;
  RefreshDisplay;
end;

constructor TDateRangeDisplay.Create(AOwner: TComponent);
begin
  inherited;
  BeginDate := Date();
  EndDate := BeginDate;
end;

procedure TDateRangeDisplay.DisplayClick(Sender: TObject);
begin
  SelectNewRange;
end;

procedure TDateRangeDisplay.SelectNewRange;
begin
  if not ReadOnly then
  with TSelectDateRangeDialog.Execute(BeginDate, EndDate) do
    if Ok then
    begin
      self.BeginDate := selectedRange.BeginDate;
      self.EndDate := selectedRange.EndDate;
      if Assigned(OnChanged) then
        OnChanged(self);
    end;
end;

end.
