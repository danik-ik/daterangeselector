program Preview;

uses
  Forms,
  SelectDateRangeDlg in '..\SelectDateRangeDlg.pas' {SelectDateRangeDialog},
  DateRange in '..\DateRange.pas',
  CalendarPair in '..\CalendarPair.pas' {DateInputPair: TFrame},
  SelectDateRangeFrm in '..\SelectDateRangeFrm.pas' {SelectDateRangeFrame: TFrame},
  DateRangeDisplayFrm in '..\DateRangeDisplayFrm.pas' {DateRangeDisplay: TFrame},
  PreviewUnit in 'PreviewUnit.pas' {PreviewForm};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TPreviewForm, PreviewForm);
  Application.Run;
end.
