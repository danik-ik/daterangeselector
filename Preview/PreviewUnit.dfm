object PreviewForm: TPreviewForm
  Left = 386
  Top = 124
  Width = 1305
  Height = 675
  Caption = 'PreviewForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 32
    Top = 264
    Width = 161
    Height = 13
    Caption = 'Label1'
  end
  inline SelectDateRangeFrame1: TSelectDateRangeFrame
    Left = 16
    Top = 8
    Width = 521
    Height = 196
    TabOrder = 0
    inherited DateInputFrom: TDateInputPair
      inherited Calendar: TMonthCalendar
        Date = 0.634572106479026800
      end
    end
    inherited DateInputTo: TDateInputPair
      inherited Calendar: TMonthCalendar
        Date = 0.634572118055075400
      end
    end
  end
  object Button1: TButton
    Left = 560
    Top = 16
    Width = 105
    Height = 25
    Caption = #1063#1090#1086' '#1074#1099#1073#1088#1072#1085#1086'?'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 560
    Top = 48
    Width = 105
    Height = 25
    Caption = #1044#1080#1072#1083#1086#1075
    TabOrder = 2
    OnClick = Button2Click
  end
  inline DateRangeDisplay1: TDateRangeDisplay
    Left = 24
    Top = 232
    Width = 195
    Height = 21
    TabOrder = 3
  end
end
