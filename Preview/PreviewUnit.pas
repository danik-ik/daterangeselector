unit PreviewUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, SelectDateRangeFrm, DateRangeDisplayFrm;

type
  TPreviewForm = class(TForm)
    SelectDateRangeFrame1: TSelectDateRangeFrame;
    Button1: TButton;
    Button2: TButton;
    DateRangeDisplay1: TDateRangeDisplay;
    Label1: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    procedure DateRangeDisplayChanged(Sender: TObject);
  public
    { Public declarations }
  end;

var
  PreviewForm: TPreviewForm;

implementation
uses
  SelectDateRangeDlg;

{$R *.dfm}

procedure TPreviewForm.Button1Click(Sender: TObject);
begin
  ShowMessage(Self.SelectDateRangeFrame1.SelectedRange.AsStr);
end;

procedure TPreviewForm.Button2Click(Sender: TObject);
begin
  with TSelectDateRangeDialog.Execute(Date(), Date()) do
  begin
    if Ok then
      ShowMessage(selectedRange.AsStr);
   end;
end;

procedure TPreviewForm.DateRangeDisplayChanged(Sender: TObject);
begin
  Label1.Caption := DateRangeDisplay1.Range.AsStr;
end;

procedure TPreviewForm.FormCreate(Sender: TObject);
begin
  SelectDateRangeFrame1.BeginDate := date();
  SelectDateRangeFrame1.EndDate := date();
  DateRangeDisplay1.OnChanged := self.DateRangeDisplayChanged;
end;

end.
