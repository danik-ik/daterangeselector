object SelectDateRangeDialog: TSelectDateRangeDialog
  Left = 386
  Top = 124
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1076#1072#1090#1091' '#1080#1083#1080' '#1080#1085#1090#1077#1088#1074#1072#1083' '#1076#1072#1090
  ClientHeight = 229
  ClientWidth = 520
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inline SelectDateRangeFrame: TSelectDateRangeFrame
    Left = 0
    Top = 0
    Width = 520
    Height = 188
    Align = alClient
    TabOrder = 0
    inherited DateInputFrom: TDateInputPair
      inherited Calendar: TMonthCalendar
        Date = 44358.634605138890000000
      end
    end
    inherited DateInputTo: TDateInputPair
      inherited Calendar: TMonthCalendar
        Date = 44358.634605162030000000
      end
    end
  end
  object Bottom: TPanel
    Left = 0
    Top = 188
    Width = 520
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object BtnHolder: TPanel
      Left = 335
      Top = 0
      Width = 185
      Height = 41
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object CancelBtn: TBitBtn
        Left = 102
        Top = 8
        Width = 75
        Height = 25
        Caption = #1054#1090#1084#1077#1085#1072
        TabOrder = 0
        Kind = bkCancel
      end
      object OkBtn: TBitBtn
        Left = 14
        Top = 8
        Width = 75
        Height = 25
        TabOrder = 1
        Kind = bkOK
      end
    end
  end
end
