unit SelectDateRangeDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CalendarPair, SelectDateRangeFrm, StdCtrls, Buttons, ExtCtrls,
  DateRange;

type
  TSelectDateRangeDialogResult = object
    Ok: boolean;
    selectedRange: TDateRange;
  end;

  TSelectDateRangeDialog = class(TForm)
    SelectDateRangeFrame: TSelectDateRangeFrame;
    Bottom: TPanel;
    BtnHolder: TPanel;
    CancelBtn: TBitBtn;
    OkBtn: TBitBtn;
    procedure FormCreate(Sender: TObject);
  private
    function GetBeginDate: TDate;
    function GetEndDate: TDate;
    procedure SetBeginDate(const Value: TDate);
    procedure SetEndDate(const Value: TDate);
  private
    property BeginDate: TDate read GetBeginDate write SetBeginDate;
    property EndDate: TDate read GetEndDate write SetEndDate;
    procedure SelectorDblClick(Sender: TObject);
  public
    class function Execute(BeginDate, EndDate: TDate): TSelectDateRangeDialogResult;
  end;

var
  SelectDateRangeDialog: TSelectDateRangeDialog;

implementation

{$R *.dfm}

{ TSelectDateRangeDialog }

class function TSelectDateRangeDialog.Execute(BeginDate,
  EndDate: TDate): TSelectDateRangeDialogResult;
var it: TSelectDateRangeDialog;
begin
  result.selectedRange.BeginDate := BeginDate;
  result.selectedRange.EndDate := EndDate;
  it := TSelectDateRangeDialog.Create(nil);
  try
    it.BeginDate := BeginDate;
    it.EndDate := EndDate;
    Result.Ok := it.ShowModal = mrOk;
    if Result.Ok then
    begin
      Result.selectedRange.BeginDate := it.BeginDate;
      Result.selectedRange.EndDate := it.EndDate;
    end;
  finally
    it.free;
  end;
end;

function TSelectDateRangeDialog.GetBeginDate: TDate;
begin
  Result := SelectDateRangeFrame.BeginDate;
end;

function TSelectDateRangeDialog.GetEndDate: TDate;
begin
  Result := SelectDateRangeFrame.EndDate;
end;

procedure TSelectDateRangeDialog.SelectorDblClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TSelectDateRangeDialog.SetBeginDate(const Value: TDate);
begin
  SelectDateRangeFrame.BeginDate := Value;
end;

procedure TSelectDateRangeDialog.SetEndDate(const Value: TDate);
begin
  SelectDateRangeFrame.EndDate := Value;
end;

procedure TSelectDateRangeDialog.FormCreate(Sender: TObject);
begin
  SelectDateRangeFrame.OnSelectorDblClick := self.SelectorDblClick;
end;

end.
