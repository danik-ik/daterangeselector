object SelectDateRangeFrame: TSelectDateRangeFrame
  Left = 0
  Top = 0
  Width = 521
  Height = 196
  TabOrder = 0
  inline DateInputFrom: TDateInputPair
    Left = 8
    Top = 8
    Width = 162
    Height = 179
    TabOrder = 0
    inherited Calendar: TMonthCalendar
      Date = 0.634571944443450800
    end
  end
  inline DateInputTo: TDateInputPair
    Left = 176
    Top = 8
    Width = 162
    Height = 179
    TabOrder = 1
    inherited Header: TLabel
      Width = 18
      Caption = #1044#1086':'
    end
    inherited Calendar: TMonthCalendar
      Date = 0.634571990740369100
    end
  end
  object MonthSelector: TListBox
    Left = 344
    Top = 8
    Width = 81
    Height = 177
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 2
    OnClick = MonthSelectorClick
    OnDblClick = MonthSelectorDblClick
  end
  object YearSelector: TListBox
    Left = 432
    Top = 8
    Width = 81
    Height = 177
    ItemHeight = 13
    MultiSelect = True
    TabOrder = 3
    OnClick = YearSelectorClick
    OnDblClick = YearSelectorDblClick
  end
end
