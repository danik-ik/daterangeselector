unit SelectDateRangeFrm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, CalendarPair, DateRange, StdCtrls;

type
  TSelectDateRangeFrame = class(TFrame)
    DateInputFrom: TDateInputPair;
    DateInputTo: TDateInputPair;
    MonthSelector: TListBox;
    YearSelector: TListBox;
    procedure MonthSelectorClick(Sender: TObject);
    procedure YearSelectorClick(Sender: TObject);
    procedure YearSelectorDblClick(Sender: TObject);
    procedure MonthSelectorDblClick(Sender: TObject);
  private
    FResult: TDateRange;
    FOnSelectorDblClick: TNotifyEvent;
    procedure RangeSelected(BeginDate, EndDate: TDate);
    procedure DateInputDblClick(Sender: TObject);
    procedure BeginDateSelected(BeginDate: TDate);
    procedure EndDateSelected(EndDate: TDate);
    procedure SetBeginDate(const Value: TDate);
    procedure SetEndDate(const Value: TDate);
    procedure FillListBoxes();
    procedure MakeTheSelectionContinuous(ListBox: TListBox);
    function GetFirstSelectedYear(): integer;
    function GetLastSelectedYear(): integer;
    function GetFirstSelectedMonth(): integer;
    function GetLastSelectedMonth(): integer;
    function FirstSelectedOf(ListBox: TListBox): integer;
    function LastSelectedOf(ListBox: TListBox): integer;
  public
    constructor Create(AOwner: TComponent); override;
    property BeginDate: TDate read FResult.BeginDate write SetBeginDate;
    property EndDate: TDate read FResult.EndDate write SetEndDate;
    property SelectedRange: TDateRange read FResult;
    property OnSelectorDblClick: TNotifyEvent read FOnSelectorDblClick write FOnSelectorDblClick;
  end;

implementation

uses DateUtils;

{$R *.dfm}

{ TSelectDateRangeFrame }

procedure TSelectDateRangeFrame.BeginDateSelected(BeginDate: TDate);
begin
  Self.BeginDate := BeginDate;
  MonthSelector.ClearSelection;
  YearSelector.ClearSelection;
end;

constructor TSelectDateRangeFrame.Create(AOwner: TComponent);
begin
  inherited;
  DateInputFrom.OnSelectRange := self.RangeSelected;
  DateInputFrom.OnSelect := Self.BeginDateSelected;
  DateInputFrom.OnCalendarDblClick := Self.DateInputDblClick;

  DateInputTo.OnSelectRange := self.RangeSelected;
  DateInputTo.OnSelect := Self.EndDateSelected;
  DateInputTo.OnCalendarDblClick := Self.DateInputDblClick;

  FillListBoxes();
end;

procedure TSelectDateRangeFrame.EndDateSelected(EndDate: TDate);
begin
  self.EndDate := EndDate;
  MonthSelector.ClearSelection;
  YearSelector.ClearSelection;
end;

procedure TSelectDateRangeFrame.RangeSelected(BeginDate,
  EndDate: TDate);
begin
  self.BeginDate := BeginDate;
  self.EndDate := EndDate;
end;

procedure TSelectDateRangeFrame.SetBeginDate(const Value: TDate);
begin
  FResult.BeginDate := Value;
  DateInputFrom.Date := Value;
  if self.BeginDate > self.EndDate then
    self.EndDate := self.BeginDate
end;

procedure TSelectDateRangeFrame.SetEndDate(const Value: TDate);
begin
  FResult.EndDate := Value;
  DateInputTo.Date := Value;
  if self.BeginDate > self.EndDate then
    self.BeginDate := self.EndDate
end;

procedure TSelectDateRangeFrame.FillListBoxes;
var
  i: integer;
  year: integer;
  addedIndex: integer;
begin
  for i := 1 to 12 do
  begin
    addedIndex := MonthSelector.Items.Add(LongMonthNames[i]);
    MonthSelector.Items.Objects[addedIndex] := TObject(i);
  end;
  year := YearOf(Date());
  for i := Year - 10 to Year + 1 do
  begin
    addedIndex := YearSelector.Items.Add(IntToStr(i));
    YearSelector.Items.Objects[addedIndex] := TObject(i);
    if i = Year then
      YearSelector.ItemIndex := addedIndex;
  end;
end;

procedure TSelectDateRangeFrame.MonthSelectorClick(Sender: TObject);
var
  Year, FirstMonth, LastMonth: integer;
begin
  MakeTheSelectionContinuous(MonthSelector);
  if (YearSelector.SelCount > 1)
  and (MonthSelector.SelCount < MonthSelector.Count) then
  begin
    YearSelector.ClearSelection;
    if YearSelector.ItemIndex >= 0 then
      YearSelector.Selected[YearSelector.ItemIndex] := true;
  end;
  if (YearSelector.SelCount = 0)
  and (YearSelector.ItemIndex >= 0 ) then
    YearSelector.Selected[YearSelector.ItemIndex] := true;
  if YearSelector.SelCount = 1 then
  begin
    Year := GetFirstSelectedYear;
    FirstMonth := GetFirstSelectedMonth;
    LastMonth := GetLastSelectedMonth;
    self.BeginDate := EncodeDate(Year, FirstMonth, 1);
    self.EndDate := EndOfTheMonth(EncodeDate(Year, LastMonth, 1));
  end;
end;

procedure TSelectDateRangeFrame.MakeTheSelectionContinuous(
  ListBox: TListBox);
var
  i, First, Last: integer;
begin
  if not ListBox.MultiSelect then exit;
  if ListBox.SelCount = 0 then exit;

  First := MaxInt;
  Last := -1;
  for i := 0 to ListBox.Count - 1 do
  begin
    if ListBox.Selected[i] then
    begin
      if i < First then First := i;
      if i > Last then Last := i;
    end;
  end;
  for i := First to Last do
    if not (ListBox.Selected[i])
      then ListBox.Selected[i] := true;
end;

procedure TSelectDateRangeFrame.YearSelectorClick(Sender: TObject);
var
  FirstYear, LastYear, FirstMonth, LastMonth: integer;
begin
  if YearSelector.SelCount = 0 then
    exit;
  MakeTheSelectionContinuous(YearSelector);
  if YearSelector.SelCount > 1 then
    MonthSelector.SelectAll;
  if MonthSelector.SelCount = 0 then
    MonthSelector.SelectAll;

  FirstYear := GetFirstSelectedYear;
  LastYear := GetLastSelectedYear;
  FirstMonth := GetFirstSelectedMonth;
  LastMonth := GetLastSelectedMonth;
  self.BeginDate := EncodeDate(FirstYear, FirstMonth, 1);
  self.EndDate := EndOfTheMonth(EncodeDate(LastYear, LastMonth, 1));
end;

procedure TSelectDateRangeFrame.YearSelectorDblClick(Sender: TObject);
var Year: integer;
begin
  MonthSelector.SelectAll;
  Year := GetFirstSelectedYear;
  self.BeginDate := EncodeDate(Year, 1, 1);
  self.EndDate := EndOfTheYear(self.BeginDate);
  if Assigned(OnSelectorDblClick) then
    OnSelectorDblClick(self);
end;

function TSelectDateRangeFrame.GetFirstSelectedMonth: integer;
begin
  result := FirstSelectedOf(MonthSelector);
end;

function TSelectDateRangeFrame.GetFirstSelectedYear: integer;
begin
  result := FirstSelectedOf(YearSelector);
end;

function TSelectDateRangeFrame.GetLastSelectedMonth: integer;
begin
  result := LastSelectedOf(MonthSelector);
end;

function TSelectDateRangeFrame.GetLastSelectedYear: integer;
begin
  result := LastSelectedOf(YearSelector);
end;

function TSelectDateRangeFrame.FirstSelectedOf(ListBox: TListBox): integer;
var i: integer;
begin
  Result := -1;
  for i := 0 to ListBox.Count - 1 do
    if ListBox.Selected[i] then
    begin
      Result := integer(ListBox.Items.Objects[i]);
      exit;
    end;
end;

function TSelectDateRangeFrame.LastSelectedOf(ListBox: TListBox): integer;
var i: integer;
begin
  Result := -1;
  for i := ListBox.Count - 1 downto 0 do
    if ListBox.Selected[i] then
    begin
      Result := integer(ListBox.Items.Objects[i]);
      exit;
    end;
end;

procedure TSelectDateRangeFrame.DateInputDblClick(Sender: TObject);
begin
  if Assigned(OnSelectorDblClick) then
    OnSelectorDblClick(self);
end;

procedure TSelectDateRangeFrame.MonthSelectorDblClick(Sender: TObject);
begin
  if Assigned(OnSelectorDblClick) then
    OnSelectorDblClick(self);
end;

end.
